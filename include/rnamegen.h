#ifndef _RNAMEGEN_H_
#define _RNAMEGEN_H_

#include <cstdio>
#include <ctime>
#include <cmath>
#include <climits>

struct letterdata{
	size_t weight, *succeede;
};

class stats{
	struct word{
		char letter;
		struct word *next=NULL, *prev=NULL;
	};
	public:
	struct letterdata *ldata;
	size_t ltotal;
	size_t cnames;
	void analyze(char **sample, size_t size);
	char *assemble(char stop);
	 stats();
	~stats();
};

#endif
