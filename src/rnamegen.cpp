#include "rnamegen.h"
#include <iostream>

#define SELECTOR sqrt(prev * curr)// * (i?1:((avgsize)/sqrt(nsize+1)) ))
//getting average name length and using it do govern the length of the output
//name feels unclean, but ever so often the generator would go haywire and
//generate abnormally long names, so this "temporary hack" seems to patch it.

#define random(x, y) (rand() % ((y) - (x) + 1) + (x))
#define abs(x) ((x)>=0?(x):-(x))

stats::stats(){
	this->ltotal = 0;
	this->cnames = 0;
	this->ldata = new letterdata[UCHAR_MAX];
	for(size_t i = 0; i < UCHAR_MAX; i++){
		this->ldata[i].weight = 0;
		this->ldata[i].succeede = new size_t[UCHAR_MAX];
		for(size_t j = 0; j < UCHAR_MAX; j++)this->ldata[i].succeede[j] = 0;
	}

}

stats::~stats(){
//	const size_t UCHAR_MAX = ((unsigned char)-1)+1;
	for(size_t i = 0; i < UCHAR_MAX; i++){
		delete [] this->ldata[i].succeede;
	}
	delete [] this->ldata;
}

void stats::analyze(char **sample, size_t size){
	//analyze letters in the names by which letter preceeds and succeedes it
	this->cnames += size;
	for(size_t i = 0; i < size; i++){
		size_t c=-1;
		do{
			c++;
			this->ldata[(size_t)sample[i][c]].weight++;
			if(sample[i][c])this->ldata[(size_t)sample[i][c]].succeede[(size_t)sample[i][c+1]]++;
			if(!c){
				this->ldata['\0'].succeede[(size_t)sample[i][c]]++;
			}
//			else this->ldata[(size_t)sample[i][c]].preceede[(size_t)sample[i][c-1]]++;
			this->ltotal++;
		}while(sample[i][c]);
	}
}

char *stats::assemble(char stop){
	//assemble a word randomly, with each letter weighted according to collected data
	unsigned long sum = 0;
	size_t nsize = 0;
	const double avgsize = (double)this->ltotal / this->cnames;
	struct word *f, *c = new struct word;
	f = c;
	do{
		long sum = 0;
		size_t prev;
		size_t curr;
		for(size_t i = 0; i < UCHAR_MAX; i++){
			if(c->prev)prev = this->ldata[(size_t)c->prev->letter].succeede[i];
			else prev = this->ldata['\0'].succeede[i];
			curr = this->ldata[i].weight;

			sum+=(SELECTOR);
		}
		long r = random(1, sum);
		for(size_t i = 0; i < UCHAR_MAX && r > 0; i++){
			c->letter = i;
			if(c->prev)prev = this->ldata[(size_t)c->prev->letter].succeede[i];
			else prev = this->ldata['\0'].succeede[i];
			curr = this->ldata[i].weight;
			r -= (SELECTOR);
		}
		nsize++;
		if(c->letter == stop){
			c->letter = '\0'; //very very dirty
			break;
		}
		else {
			c->next = new struct word;
			c->next->prev = c;
			c = c->next;
		}
	}while(c->letter);
	char *out = new char[nsize + 1];
	out[nsize] = '\0';
	c = f;
	for(size_t i = 0; i < nsize; i++){
		out[i] = c->letter;
		if(c->next){
			c = c->next;
			delete c->prev;
		}
	}
	delete c;
	return out;
}
